import { BTCWebPage } from './app.po';

describe('btc-web App', () => {
  let page: BTCWebPage;

  beforeEach(() => {
    page = new BTCWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
