import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes }   from '@angular/router';

import { AppComponent } from './app.component';

import { LatestOrdersComponent } from './orders/latest_orders.component';
import { LatestTransactionsComponent } from './transactions/latest_transactions.component';
import { MarketStatusComponent } from './markets/market_status.component';

@NgModule({
  declarations: [
    AppComponent,
    LatestOrdersComponent,
    LatestTransactionsComponent,
    MarketStatusComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
